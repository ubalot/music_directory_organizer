"""

"""
import os
import re

from src.album.subalbum import SubAlbum
from src.announcer import Announcer
from src.constants import ALBUM_PATTERN, MUSIC_FILE_EXTENSIONS, SUB_ALBUM_PATTERN
from src.helpers import all_content_is_dir, extract_dirname, shrink_whitespaces, split_extension, sleep
from src.monads import listdir, rename, remove
from src.song.albumsong import AlbumSong


class Album:
    """ """

    def __init__(self, artists_folder: str, artist_path: str, album: str) -> None:
        self.root = artists_folder

        self.artist_path = artist_path
        self.artist = extract_dirname(artist_path)

        self.album_path = os.path.join(artist_path, album)
        self.album = album

    def make_song(self, song: str) -> AlbumSong:
        """ Return a Song object for this album location. """
        return AlbumSong(self.root, self.artist, self.album, song)

    def scan(self) -> None:
        """ """
        Announcer.info(f'scanning {self.album} ...\n')

        self.__remove_extraneous_files()

        # Check if album must be renamed.
        if not ALBUM_PATTERN.fullmatch(self.album):
            self._rename()

        if all_content_is_dir(self.album_path):
            # sub album scan
            for sub_album_name in listdir(self.album_path):
                sub_album = SubAlbum(self.root, self.artist_path, self.album, sub_album_name)
                sub_album.scan()
                del sub_album
        else:
            # album songs scan
            for song in listdir(self.album_path):
                song = self.make_song(song)
                song.scan()
                del song

    def _rename(self) -> None:
        """ Rename album filename to standard album name """
        album_title = shrink_whitespaces(self.album)

        digits = re.findall(r'\d{4}', album_title)

        if digits:
            year = digits[0]

            words = re.findall(r"[a-zA-Z\-]+", album_title)
            if year in words:
                words.remove(year)

            new_album_title = f"({year}) {' '.join(words)}"
            new_album_path = os.path.join(self.artist_path, new_album_title)

            if Announcer.ask_if_rename(self.album_path, new_album_path):
                rename(self.album_path, new_album_path)
                self.album_path = new_album_path
                self.album = new_album_title

        else:
            Announcer.error(f"'{self.album}' by '{self.artist}' doesn't match album regex.")
            sleep()

    def __remove_extraneous_files(self) -> None:
        """
        Remove non-song files from directory. User will be prompt before removing.
        """
        for item in listdir(self.album_path):
            item_path = os.path.join(self.album_path, item)

            # handle directory
            if os.path.isdir(item_path):
                if not SUB_ALBUM_PATTERN.fullmatch(item):
                    Announcer.ask_if_delete(item_path)

            # handle file
            else:
                title, file_extension = split_extension(item)

                if file_extension not in MUSIC_FILE_EXTENSIONS:

                    # music file with extension in uppercase
                    if file_extension.lower() in MUSIC_FILE_EXTENSIONS:
                        new_name = title + '.' + file_extension.lower()
                        new_path = os.path.join(self.album_path, new_name)

                        if Announcer.ask_if_rename(item_path, new_path):
                            rename(item_path, new_path)

                    # not music file
                    else:
                        if Announcer.ask_if_delete(item_path):
                            remove(item_path)
